<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Contracts\Console\Kernel;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication, RefreshDatabase;

    protected function setUp() :void
    {
        parent::setUp();
        \DB::setDefaultConnection([
            'sqlite' => [
                'driver' => 'sqlite',
                'database' => 'codetest.db'
            ]
        ]);
    }

    /**
     * Define hooks to migrate the database before and after each test.
     *
     * @return void
     */
    public function refreshDatabase()
    {
        $this->artisan('migrate:refresh', [
            '--database' => 'sqlite'
        ]);
    }
}
