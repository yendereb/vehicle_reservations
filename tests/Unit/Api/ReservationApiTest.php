<?php

namespace Tests\Unit;

use Illuminate\Support\Str;
use Carbon\Carbon;
use Tests\TestCase;

class ReservationApiTest extends TestCase
{
    protected $rData = [];
    protected $client;
    protected $brand;
    protected $vehicle;

    public function setUp(): void
    {
        parent::setUp();
        $this->rData = [
            'date_start' => Carbon::now()->format('Y-m-d'),
            'client_id'  => null,
            'vehicle_id' => null,
            'status_id'  => 1
        ];

        $responseClient = $this->json('POST', 'api/clients', [
            'first_name' => Str::random(10),
            'last_name'  => Str::random(12),
            'phone'      => '+15555555',
            'address'    => 'Address Test',
            'code'       => uniqid(),
            'user_id'    => 1
        ])->getContent();
        $responseClient = json_decode($responseClient, true);
        $this->client   = $responseClient['client'];

        $responseBrand = $this->json('GET', 'api/brands');
        $responseBrand = json_decode($responseBrand->getContent(), true);
        $this->brand = current($responseBrand['brands']);

        $responseVehicle = $this->json('POST', 'api/vehicles', [
            'registration' => Str::random(8),
            'model'        => 'sedan',
            'year'         => 2019,
            'seats'        => 5,
            'status'       => 'available',
            'brand_id'     => $this->brand['id']
        ])->getContent();
        $responseVehicle = json_decode($responseVehicle, true);
        $this->vehicle   = $responseVehicle['vehicle'];
    }

    public function testValidationCreateReservation()
    {
        $response = $this->json('POST', 'api/reservations');

        $response
            ->assertStatus(422)
            ->assertJson([
                'message' => 'The given data was invalid.'
            ]);

        $responseVehicle = $this->json('POST', 'api/vehicles', [
            'registration' => Str::random(8),
            'model'        => 'sedan',
            'year'         => 2016,
            'seats'        => 4,
            'status'       => 'discontinued',
            'brand_id'     => $this->brand['id']
        ])->getContent();
        $responseVehicle = json_decode($responseVehicle, true);
        $vehicle = $responseVehicle['vehicle'];

        $this->rData['client_id'] = $this->client['id'];
        $this->rData['vehicle_id'] = $vehicle['id'];

        $response = $this->json('POST', 'api/reservations', $this->rData);
        $response
            ->assertStatus(404)
            ->assertJson([
                'success' => false,
                'error' => 'Vehicle Not Found'
            ]);
    }

    public function testCreateReservation()
    {
        $this->rData['client_id'] = $this->client['id'];
        $this->rData['vehicle_id'] = $this->vehicle['id'];

        $response = $this->json('POST', 'api/reservations', $this->rData);
        $response
            ->assertStatus(200)
            ->assertJson([
                'success' => true
            ]);
    }

    public function testListReservations()
    {
        $responseVehicle = $this->json('POST', 'api/vehicles', [
            'registration' => Str::random(8),
            'model'        => 'truck',
            'year'         => 2016,
            'seats'        => 4,
            'status'       => 'available',
            'brand_id'     => $this->brand['id']
        ])->getContent();
        $responseVehicle = json_decode($responseVehicle, true);
        $vehicle = $responseVehicle['vehicle'];

        $this->rData['vehicle_id'] = $vehicle['id'];
        $this->rData['client_id'] = $this->client['id'];

        $response = $this->json('POST', 'api/reservations', $this->rData);
        $response
            ->assertStatus(200)
            ->assertJson([
                'success' => true
            ]);

        $responseShow = $this->json('GET', 'api/reservations');
        $reservations = json_decode($responseShow->getContent(), true);
        $lastReservation = end($reservations['reservations']);

        $this->assertEquals(
            $this->rData['date_start'],
            Carbon::parse(
                $lastReservation['date_start']['date']
            )->format('Y-m-d')
        );
    }

    public function testShowValidationReservations()
    {
        $response = $this->json('GET', 'api/reservations/9999999999999999');
        $response
            ->assertStatus(404)
            ->assertJson([
                'success' => false,
                'error' => 'Not found'
            ]);
    }

    public function testShowReservations()
    {
        $response = $this->json('GET', 'api/reservations');
        $response = json_decode($response->getContent(), true);

        $reservations = $response['reservations'];
        $last = end($reservations);

        $responseShow = $this->json('GET', 'api/reservations/' . $last['id']);
        $responseShow = json_decode($responseShow->getContent(), true);

        $this->assertEquals(
            $last['date_start'],
            $responseShow['reservation']['date_start']
        );
    }

    public function testUpdateReservations()
    {
        $this->rData['date_end'] = Carbon::now()->addDays(10)->format('Y-m-d');

        $response = $this->json('GET', 'api/reservations');
        $response = json_decode($response->getContent(), true);
        $lastReservation = end($response['reservations']);

        $response = $this->json(
            'PUT',
            'api/reservations/' . $lastReservation['id'],
            $this->rData
        );
        $response->assertStatus(200)
            ->assertJson([
                'success' => true
            ]);
    }

    public function testRemoveReservations()
    {
        $responseVehicle = $this->json('POST', 'api/vehicles', [
            'registration' => Str::random(8),
            'model'        => 'pickup',
            'year'         => 2015,
            'seats'        => 3,
            'status'       => 'available',
            'brand_id'     => $this->brand['id']
        ])->getContent();
        $responseVehicle = json_decode($responseVehicle, true);
        $vehicle = $responseVehicle['vehicle'];

        $this->rData['vehicle_id'] = $vehicle['id'];
        $this->rData['client_id'] = $this->client['id'];

        $response = $this->json('POST', 'api/reservations', $this->rData);
        $response = json_decode($response->getContent(), true);
        $reservation = $response['reservation'];

        $response = $this->json(
            'DELETE',
            '/api/reservations/' . $reservation['id']
        )->assertStatus(200)
        ->assertJson([
            'success' => true
        ]);
    }

    public function testShowAvailables()
    {
        $vehicle0 = $this->json('POST', 'api/vehicles', [
            'registration' => Str::random(8),
            'model'        => 'pickup',
            'year'         => 2015,
            'seats'        => 3,
            'status'       => 'available',
            'brand_id'     => $this->brand['id']
        ])->getContent();
        $vehicle1 = $this->json('POST', 'api/vehicles', [
            'registration' => Str::random(8),
            'model'        => 'pickup',
            'year'         => 2015,
            'seats'        => 3,
            'status'       => 'available',
            'brand_id'     => $this->brand['id']
        ])->getContent();
        $vehicle1 = json_decode($vehicle1, true);
        $vehicle1 = $vehicle1['vehicle'];

        $response = $this->json('GET', 'api/reservations/availables')
            ->assertStatus(200)
            ->getContent();
        $response = json_decode($response, true);
        $response = end($response['vehicles']);

        $this->assertEquals(
            $vehicle1['registration'],
            $response['registration']
        );
    }

    public function testChangeReservationStatus()
    {
        $vehicle0 = $this->json('POST', 'api/vehicles', [
            'registration' => Str::random(8),
            'model'        => 'pickup',
            'year'         => 2015,
            'seats'        => 3,
            'status'       => 'available',
            'brand_id'     => $this->brand['id']
        ])->getContent();
        $vehicle = json_decode($vehicle0, true);
        $vehicle = $vehicle['vehicle'];

        $this->rData['client_id']  = $this->client['id'];
        $this->rData['vehicle_id'] = $vehicle['id'];
        $this->rData['status_id']  = 1; //Requested

        $response = $this->json('POST', 'api/reservations', $this->rData)
            ->assertStatus(200)
            ->getContent();
        $response = json_decode($response, true);
        $reservation = $response['reservation'];

        $response = $this->json(
            'POST',
            'api/reservations/change',
            [
                'id'     => $reservation['id'],
                'status' => 'Approved'
            ]);

        $responseShow = $this->json('GET', 'api/reservations/' . $reservation['id']);
        $responseShow = json_decode($responseShow->getContent(), true);
        $responseShow = $responseShow['reservation'];

        $this->assertEquals(
            2,
            $responseShow['status']['id']
        );

        $response = $this->json(
            'POST',
            'api/reservations/change',
            [
                'id'     => $reservation['id'],
                'status' => 'Cancelled'
            ]);

        $responseShow = $this->json('GET', 'api/reservations/' . $reservation['id']);
        $responseShow = json_decode($responseShow->getContent(), true);
        $responseShow = $responseShow['reservation'];

        $this->assertEquals(
            4,
            $responseShow['status']['id']
        );
    }
}