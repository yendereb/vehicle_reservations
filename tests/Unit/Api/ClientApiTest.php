<?php

namespace Tests\Unit;

use Illuminate\Support\Str;
use Tests\TestCase;

class ClientApiTest extends TestCase
{
    protected $vData = [];

    public function setUp(): void
    {
        parent::setUp();
        $this->vData = [
            'first_name' => Str::random(10),
            'last_name'  => Str::random(12),
            'phone'      => '+15555555',
            'address'    => 'Address Test',
            'code'       => uniqid(),
            'user_id'    => 1
        ];
    }

    public function testValidationCreateClient()
    {
        $response = $this->json('POST', 'api/clients');

        $response
            ->assertStatus(422)
            ->assertJson([
                'message' => 'The given data was invalid.'
            ]);
    }

    public function testCreateClient()
    {
        $response = $this->json('POST', 'api/clients', $this->vData);
        $response
            ->assertStatus(200)
            ->assertJson([
                'success' => true
            ]);
    }

    public function testListClients()
    {
        $this->vData['code'] = uniqid();
        $response = $this->json('POST', 'api/clients', $this->vData);
        $response
            ->assertStatus(200)
            ->assertJson([
                'success' => true
            ]);

        $responseShow = $this->json('GET', 'api/clients');
        $clients = json_decode($responseShow->getContent(), true);

        $this->assertEquals(
            $this->vData['code'],
            end($clients['clients'])['code']
        );
    }

    public function testShowValidationClients()
    {
        $response = $this->json('GET', 'api/clients/9999999999999999');
        $response
            ->assertStatus(404)
            ->assertJson([
                'success' => false,
                'error' => 'Not found'
            ]);
    }

    public function testShowClients()
    {
        $response = $this->json('GET', 'api/clients');
        $response = json_decode($response->getContent(), true);
        $clients = $response['clients'];
        $last = end($clients);

        $responseShow = $this->json('GET', 'api/clients/' . $last['id']);
        $responseShow = json_decode($responseShow->getContent(), true);

        $this->assertEquals(
            $last['code'],
            $responseShow['client']['code']
        );
    }

    public function testUpdateClients()
    {
        $this->vData['code'] = 'xxTestCODExx';

        $response = $this->json('GET', 'api/clients');
        $response = json_decode($response->getContent(), true);
        $lastClient = end($response['clients']);

        $response = $this->json(
            'PUT',
            'api/clients/' . $lastClient['id'],
            $this->vData
        );
        $response->assertStatus(200)
            ->assertJson([
                'success' => true
            ]);
    }

    public function testRemoveClients()
    {
        $response = $this->json('GET', 'api/clients');
        $response = json_decode($response->getContent(), true);
        $last = end($response['clients']);

        $response = $this->json('DELETE', '/api/clients/' . $last['id']);
        $response
            ->assertStatus(200)
            ->assertJson([
                'success' => true
            ]);
    }
}