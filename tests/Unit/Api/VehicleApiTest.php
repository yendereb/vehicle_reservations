<?php

namespace Tests\Unit;

use Illuminate\Support\Str;
use Tests\TestCase;

class VehicleApiTest extends TestCase
{
    public function testValidationCreateVehicle()
    {
        $response = $this->json('POST', 'api/vehicles');

        $response
            ->assertStatus(422)
            ->assertJson([
                'message' => 'The given data was invalid.'
            ]);
    }

    public function testCreateVehicle()
    {
        $vData = [
            'registration' => Str::random(8),
            'model'        => 'sedan',
            'year'         => 2019,
            'seats'        => 5,
            'status'       => 'available',
            'brand_id'     => 1
        ];

        $response = $this->json('POST', 'api/vehicles', $vData);
        $response
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
                'vehicle' => [
                    'registration' => $vData['registration']
                ]
            ]);
    }

    public function testListVehicles()
    {
        $vData = [
            'registration' => Str::random(8),
            'model'        => 'sedan',
            'year'         => 2019,
            'seats'        => 5,
            'status'       => 'available',
            'conditions'   => 'nuevo',
            'brand_id'     => 1
        ];
        $response = $this->json('POST', 'api/vehicles', $vData);
        $response
            ->assertStatus(200)
            ->assertJson([
                'success' => true
            ]);

        $responseShow = $this->json('GET', 'api/vehicles');
        $responseShow->assertJsonFragment([
            'registration' => $vData['registration']
        ]);

        $vehicles = json_decode($responseShow->getContent(), true);

        $this->assertEquals(
            $vData['registration'],
            end($vehicles['vehicles'])['registration']
        );
    }

    public function testShowValidationVehicles()
    {
        $response = $this->json('GET', 'api/vehicles/9999999999999999');
        $response
            ->assertStatus(404)
            ->assertJson([
                'success' => false,
                'error' => 'Not found'
            ]);
    }

    public function testShowVehicles()
    {
        $response = $this->json('GET', 'api/vehicles');
        $response = json_decode($response->getContent(), true);
        $vehicles = $response['vehicles'];
        $last = end($vehicles);

        $responseShow = $this->json('GET', 'api/vehicles/' . $last['id']);
        $responseShow = json_decode($responseShow->getContent(), true);

        $this->assertEquals(
            $last['registration'],
            $responseShow['vehicle']['registration']
        );
    }

    public function testUpdateVehicles()
    {
        $vData = [
            'registration' => Str::random(8),
            'model'        => 'sedan',
            'year'         => 2019,
            'seats'        => 5,
            'status'       => 'discontinued',
            'conditions'   => 'cambiado',
            'brand_id'     => 1
        ];

        $response = $this->json('GET', 'api/vehicles');
        $response = json_decode($response->getContent(), true);
        $lastVehicle = end($response['vehicles']);

        $response = $this->json(
            'PUT',
            'api/vehicles/' . $lastVehicle['id'],
            $vData
        );
        $response->assertStatus(200)
            ->assertJson([
                'success' => true
            ]);
    }

    public function testRemoveVehicles()
    {
        $response = $this->json('GET', 'api/vehicles');
        $response = json_decode($response->getContent(), true);
        $last = end($response['vehicles']);

        $response = $this->json('DELETE', '/api/vehicles/' . $last['id']);
        $response
            ->assertStatus(200)
            ->assertJson([
                'success' => true
            ]);
    }
}