<?php

namespace Tests\Unit;

use Illuminate\Support\Str;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class BrandApiTest extends TestCase
{
    protected function setUp() :void
    {
        parent::setUp();
        $this->RefreshDatabase();
    }

    public function testValidationCreateBrand()
    {
        $response = $this->json('POST', '/api/brands');

        $response
            ->assertStatus(422)
            ->assertJson([
                'message' => 'The given data was invalid.'
            ]);
    }

    public function testCreateBrand()
    {
        $brandName = Str::random(10);
        $response = $this->json(
            'POST',
            'api/brands',
            ['name' => $brandName]
        );

        $response
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
                'brand' => [
                    'name' => $brandName
                ]
            ]);
    }

    public function testListBrands()
    {
        $brandName = Str::random(10);
        $response = $this->json('POST', '/api/brands', ['name' => $brandName]);

        $response
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
                'brand' => [
                    'name' => $brandName
                ]
            ]);

        $response = $this->json('GET', '/api/brands');
        $response->assertJsonFragment([
            'name' => $brandName
        ]);
    }

    public function testShowValidationBrands()
    {
        $response = $this->json('GET', '/api/brands/9999999999999999');
        $response
            ->assertStatus(404)
            ->assertJson([
                'success' => false,
                'error' => 'Not found'
            ]);
    }

    public function testShowBrands()
    {
        $response = $this->json('GET', 'api/brands');
        $response = json_decode($response->getContent(), true);
        $brands = $response['brands'];
        $last = end($brands);

        $responseShow = $this->json('GET', 'api/brands/' . $last['id']);
        $responseShow = json_decode($responseShow->getContent(), true);

        $this->assertEquals(
            $last['name'],
            $responseShow['brand']['name']
        );
    }

    public function testUpdateBrands()
    {
        $brandName  = Str::random(10);
        $updateName = Str::random(12);

        $response = $this->json('POST', '/api/brands', ['name' => $brandName]);
        $response
            ->assertStatus(200)
            ->assertJson([
                'success' => true,
                'brand' => [
                    'name' => $brandName
                ]
            ]);

        $response = $this->json('GET', 'api/brands');
        $response = json_decode($response->getContent(), true);
        $brands = $response['brands'];
        $last = end($brands);

        $response = $this->json(
            'PUT',
            '/api/brands/' . $last['id'],
            ['name' => $updateName]
        );
        $response
            ->assertStatus(200)
            ->assertJson([
                'success' => true
            ]);

        $response = $this->json('GET', '/api/brands/' . $last['id']);
        $response->assertJsonFragment([
            'name' => $updateName
        ]);
    }

    public function testRemoveBrands()
    {
        $brandName = Str::random(10);
        $response  = $this->json('POST', '/api/brands', ['name' => $brandName]);
        $saveResp  = json_decode($response->getContent(), true);
        $saveResp  = $saveResp['brand'];

        $response = $this->json('DELETE', '/api/brands/' . $saveResp['id']);
        $response
            ->assertStatus(200)
            ->assertJson([
                'success' => true
            ]);
    }
}