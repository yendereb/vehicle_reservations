<?php

namespace Tests\Unit;

use App\Repositories\ReservationStatusRepo;
use Tests\TestCase;

class ReservationStatusTest extends TestCase
{
    protected $reservationStatusRepo;

    protected function setUp() :void
    {
        parent::setUp();
        $this->reservationStatusRepo = new ReservationStatusRepo('sqlite');

        $reservationStatus = $this->reservationStatusRepo->create([
            'name' => 'Status Test',
        ]);
    }

    /**
     * Should create a new reservation status
     *
     * @return void
     */
    public function testCreateNewReservationStatus()
    {
        $attrs = $this->reservationStatusRepo->getAllAttributes();

        $this->assertCount(1, $this->reservationStatusRepo->list());
        $this->assertIsArray($attrs);
        $this->assertEquals('Status Test', $attrs['name']);
    }

    /**
     * Should update the reservation status data
     *
     * @return void
     */
    public function testUpdateReservationStatus()
    {
        $reservationStatus = $this->reservationStatusRepo->find(1);

        $attrs = $reservationStatus->getAllAttributes();
        $this->assertEquals('Status Test', $attrs['name']);

        $reservationStatus->update([
            'name' => 'Status Changed'
        ]);

        $attrs = $reservationStatus->getAllAttributes();
        $this->assertEquals('Status Changed', $attrs['name']);
    }
}
