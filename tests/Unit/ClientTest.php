<?php

namespace Tests\Unit;

use App\Repositories\UserRepo;
use App\Repositories\ClientRepo;
use Illuminate\Support\Str;
use App\Entities\User;
use Tests\TestCase;

class ClientTest extends TestCase
{
    protected $userRepo;
    protected $clientRepo;

    protected function setUp() :void
    {
        parent::setUp();
        $this->userRepo = new ClientRepo('sqlite');
        $this->clientRepo = new ClientRepo('sqlite');
    }

    /**
     * Should create a new client
     *
     * @return void
     */
    public function testCreateNewClient()
    {
        $user = $this->userRepo->setEntity(entity(User::class)->make());
        $user->save();
        $client = $this->clientRepo->create([
            'first_name' => 'Name Test',
            'last_name'  => 'Last Name Test',
            'phone'      => '5555555',
            'address'    => 'Address Test',
            'code'       => Str::random(6),
            'user'       => $this->userRepo
        ]);

        $lastClient = $client->getAllAttributes();

        $this->assertCount(1, $this->clientRepo->list());
        $this->assertIsArray($lastClient);
        $this->assertEquals('Name Test', $lastClient['first_name']);
        $this->assertEquals('5555555', $lastClient['phone']);
        $this->assertEquals(
            $user->get('email'),
            $client->get('user')->getEmail()
        );
    }

    /**
     * Should update the client
     *
     * @return void
     */
    public function testUpdateClient()
    {
        $this->userRepo->setEntity(entity(User::class)->make())->save();
        $client = $this->clientRepo->create([
            'first_name' => 'Name Test',
            'last_name'  => 'Last Name Test',
            'phone'      => '5555555',
            'address'    => 'Address Test',
            'code'       => Str::random(6),
            'user'       => $this->userRepo
        ]);

        $lastClient = $client->getAllAttributes();
        $this->assertEquals('Name Test', $lastClient['first_name']);
        $this->assertEquals('5555555', $lastClient['phone']);

        $client->update([
            'first_name' => 'Name Changed',
            'phone'      => '999999999999',
        ]);

        $lastClient = $client->getAllAttributes();
        $this->assertEquals('Name Changed', $lastClient['first_name']);
        $this->assertEquals('999999999999', $lastClient['phone']);
    }
}
