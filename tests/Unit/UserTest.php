<?php

namespace Tests\Unit;

use App\Repositories\UserRepo;
use Illuminate\Support\Str;
use App\Entities\User;
use Tests\TestCase;

class UserTest extends TestCase
{
    protected $userRepo;

    protected function setUp() :void
    {
        parent::setUp();
        $this->userRepo = new UserRepo('sqlite');
    }

    /**
     * Should create a new user
     *
     * @return void
     */
    public function testCreateNewUser()
    {
        $this->userRepo->setEntity(entity(User::class)->make())->save();
        $this->userRepo->create([
            'email'             => 'examplemail@server.net',
            'password'          => 'test_password',
            'email_verified_at' => \Carbon\Carbon::now(),
            'api_token'         => Str::random(80),
            'remember_token'    => Str::random(10)
        ]);

        $lastUser = $this->userRepo->getAllAttributes();

        $this->assertCount(2, $this->userRepo->list());
        $this->assertIsArray($lastUser);
        $this->assertEquals('examplemail@server.net', $lastUser['email']);
    }

    /**
     * Should update the user
     *
     * @return void
     */
    public function testUpdateUser()
    {
        $user = $this->userRepo->create([
            'email'             => 'examplemail@server.net',
            'password'          => 'test_password',
            'email_verified_at' => \Carbon\Carbon::now(),
            'api_token'         => Str::random(80),
            'remember_token'    => Str::random(10)
        ]);

        $lastUser = $user->getAllAttributes();
        $this->assertEquals('examplemail@server.net', $lastUser['email']);
        $this->assertEquals('test_password', $lastUser['password']);

        $user->update([
            'email'    => 'testemail@server.net',
            'password' => 'xx01234460',
        ]);

        $lastUser = $user->getAllAttributes();
        $this->assertEquals('testemail@server.net', $lastUser['email']);
        $this->assertEquals('xx01234460', $lastUser['password']);
    }
}
