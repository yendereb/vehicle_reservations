<?php

namespace Tests\Unit;

use App\Repositories\BrandRepo;
use Illuminate\Foundation\Testing\WithFaker;
use App\Entities\Brand;
use Tests\TestCase;

class BrandTest extends TestCase
{
    protected $brandName   = 'Test Name';
    protected $anotherName = 'Another Name';

    protected function setUp() :void
    {
        parent::setUp();
        $this->brandRepo = new BrandRepo('sqlite');

        $this->brandRepo->create(['name' => 'Brand 1']);
        $this->brandRepo->create(['name' => $this->brandName]);
    }

    /**
     * Should create a new brand
     *
     * @return void
     */
    public function testCreateNewBrand()
    {
        $brands = $this->brandRepo->list();
        $lastBrand = end($brands);

        $this->assertEquals($this->brandName, $lastBrand['name']);
    }

    /**
     * Should update a brand
     *
     * @return void
     */
    public function testUpdateNewBrand()
    {
        $this->brandRepo->update(['name' => $this->anotherName]);

        $brands = $this->brandRepo->list();
        $lastBrand = end($brands);

        $this->assertEquals($this->anotherName, $lastBrand['name']);
    }

    /**
     * Should remove a brand
     *
     * @return void
     */
    public function testRemoveABrandById()
    {
        $brands = $this->brandRepo->list();
        $this->assertEquals(2, count($brands));

        $lastBrand = end($brands);
        $this->brandRepo->delete($lastBrand['id']);

        $brands = $this->brandRepo->list();
        $this->assertEquals(1, count($brands));
    }

    /**
     * Should find a row
     *
     * @return void
     */
    public function testFindByAttribute()
    {
        $this->brandRepo->create(['name' => 'Another Brand Test']);

        $brand = $this->brandRepo->findOneBy(['name' => 'Another Brand Test']);
        $brand = $brand->getAllAttributes();

        $this->assertIsArray($brand);
        $this->assertEquals('Another Brand Test', $brand['name']);
    }

    /**
     * Should test Brand set validation
     *
     * @return void
     */
    public function testBrandSetValidation()
    {
        $obj1 = $this->brandRepo->set('name', 'Another Brand Test');
        $obj2 = $this->brandRepo->set('nameFoo', 'Another Brand Test');

        $this->assertIsObject($obj1);
        $this->assertNull($obj2);
    }

    /**
     * Should test Brand set Entity
     *
     * @return void
     */
    public function testBrandSetEntity()
    {
        $entBrand = new Brand();
        $this->brandRepo->setEntity($entBrand);

        $this->assertEquals($entBrand, $this->brandRepo->getEntity());
    }
}
