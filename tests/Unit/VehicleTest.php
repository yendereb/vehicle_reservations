<?php

namespace Tests\Unit;

use App\Repositories\BrandRepo;
use App\Repositories\VehicleRepo;
use Illuminate\Foundation\Testing\WithFaker;
use App\Entities\Vehicle;
use Tests\TestCase;

class VehicleTest extends TestCase
{
    protected $brandRepo;
    protected $vehicleRepo;

    protected function setUp() :void
    {
        parent::setUp();

        $this->brandRepo = new BrandRepo('sqlite');
        $this->vehicleRepo = new VehicleRepo('sqlite');

        $this->brandRepo->create(['name' => 'Brand Test']);
    }

    /**
     * Should create a new brand
     *
     * @return void
     */
    public function testCreateNewVehicle()
    {
        $this->vehicleRepo->setEntity(entity(Vehicle::class)->make())->save();
        $vehicle = $this->vehicleRepo->create([
            'registration' => '11234567',
            'model'        => 'pickup',
            'seats'        => 3,
            'year'         => 2019,
            'conditions'   => 'Condition Test',
            'status'       => 'available'
        ]);

        $attrs = $vehicle->getAllAttributes();

        $this->assertCount(2, $this->vehicleRepo->list());
        $this->assertIsArray($attrs);
        $this->assertEquals('11234567', $attrs['registration']);
        $this->assertEquals('pickup', $attrs['model']);
        $this->assertEquals('Condition Test', $attrs['conditions']);
    }

    /**
     * Should create a new brand
     *
     * @return void
     */
    public function testSetBrandToVehicle()
    {
        $vEntity = entity(Vehicle::class)->make();
        $this->vehicleRepo->setEntity($vEntity)->save();

        $vehicle = $this->vehicleRepo->find(1);
        $vehicle->set('brand', $this->brandRepo);

        $this->assertEquals(
            $vEntity->getRegistration(),
            $vehicle->get('registration')
        );

        $this->assertEquals(
            'Brand Test',
            $vehicle->get('brand')->getName()
        );
    }

    /**
     * Should update a vehicle
     *
     * @return void
     */
    public function testUpdateNewVehicle()
    {
        $vEntity = entity(Vehicle::class)->make();
        $this->vehicleRepo->setEntity($vEntity)->save();

        $this->vehicleRepo->update(['registration' => 'xxTestRegistrationxx']);

        $this->assertEquals(
            'xxTestRegistrationxx',
            $this->vehicleRepo->get('registration')
        );
    }

    /**
     * Should remove a vehicle
     *
     * @return void
     */
    public function testRemoveAVehicle()
    {
        $vEntity = entity(Vehicle::class)->make();
        $this->vehicleRepo->setEntity($vEntity)->save();

        $vehicles = $this->vehicleRepo->list();
        $this->assertEquals(1, count($vehicles));

        $this->vehicleRepo->delete($vehicles[0]['id']);

        $vehicles = $this->vehicleRepo->list();
        $this->assertEquals(0, count($vehicles));
    }

    /**
     * Should list all vehicles and its brands
     *
     * @return void
     */
    public function testListsVehicles()
    {
        $this->vehicleRepo->setEntity(
            entity(Vehicle::class)->make()
        )->save();
        $this->vehicleRepo->set('brand', $this->brandRepo);

        $brand2   = $this->brandRepo->create(['name' => 'Another Brand']);
        $vehicle2 = $this->vehicleRepo->setEntity(
            entity(Vehicle::class)->make()
        );
        $vehicle2->set('brand', $brand2)->save();

        $vehicles = $this->vehicleRepo->with('brand')->list();

        $this->assertEquals('Brand Test', $vehicles[0]['brand']['name']);
        $this->assertEquals('Another Brand', $vehicles[1]['brand']['name']);
    }
}
