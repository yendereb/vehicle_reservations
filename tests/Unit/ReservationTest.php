<?php

namespace Tests\Unit;

use App\Repositories\UserRepo;
use App\Repositories\ClientRepo;
use App\Repositories\BrandRepo;
use App\Repositories\VehicleRepo;
use App\Repositories\ReservationsRepo;
use App\Repositories\ReservationStatusRepo;
use App\Entities\User;
use App\Entities\Brand;
use App\Entities\Vehicle;
use Carbon\Carbon;
use Tests\TestCase;

class ReservationTest extends TestCase
{
    protected $userRepo;
    protected $clientRepo;
    protected $brandRepo;
    protected $vehicleRepo;
    protected $reservationRepo;
    protected $reservationStatusRepo;

    protected function setUp() :void
    {
        parent::setUp();

        $this->userRepo = new UserRepo('sqlite');
        $this->clientRepo = new ClientRepo('sqlite');
        $this->brandRepo = new BrandRepo('sqlite');
        $this->vehicleRepo = new VehicleRepo('sqlite');
        $this->reservationRepo = new ReservationsRepo('sqlite');
        $this->reservationStatusRepo = new reservationStatusRepo('sqlite');

        $this->userRepo->setEntity(entity(User::class)->make())->save();
        $this->clientRepo->create([
            'first_name' => 'Client Test',
            'last_name'  => 'Last Name Test',
            'phone'      => '5555555',
            'address'    => 'Address Test',
            'code'       => uniqid(),
            'user'       => $this->userRepo
        ]);
        $this->brandRepo->setEntity(entity(Brand::class)->make())->save();
        $this->vehicleRepo->setEntity(entity(Vehicle::class)->make());
        $this->vehicleRepo->set('brand', $this->brandRepo)->save();
        $this->reservationStatusRepo->create(['name' => 'Approved']);
        $this->reservationStatusRepo->create(['name' => 'Declined']);
        $this->reservationStatusRepo->create(['name' => 'Cancelled']);
        $this->reservationStatusRepo->create(['name' => 'Ended']);
        $this->reservationStatusRepo->create(['name' => 'Requested']);

        $start = Carbon::now();
        $end   = $start;
        $end->addDays(10);

        $this->reservationRepo->create([
            'date_start' => $start,
            'date_end'   => $end,
            'vehicle'    => $this->vehicleRepo,
            'client'     => $this->clientRepo,
            'status'     => $this->reservationStatusRepo
        ]);
    }

    /**
     * Should create a new brand
     *
     * @return void
     */
    public function testCreateNewReservation()
    {
        $attrs = $this->reservationRepo->getAllAttributes();

        $this->assertCount(1, $this->reservationRepo->list());
        $this->assertIsArray($attrs);
        $this->assertEquals(
            'Client Test',
            $this->reservationRepo->get('client')->getFirst_name()
        );
        $this->assertEquals(
            $this->vehicleRepo->get('registration'),
            $this->reservationRepo->get('vehicle')->getRegistration()
        );
        $this->assertEquals(
            'Requested',
            $this->reservationRepo->get('status')->getName()
        );
    }

    /**
     * Should update a reservation
     *
     * @return void
     */
    public function testUpdateReservation()
    {
        $dateChanged = Carbon::now()->addDays(20);
        $this->reservationRepo->update([
            'date_end' => $dateChanged,
            'status' => $this->reservationStatusRepo->find(3)
        ]);

        $this->reservationRepo->find(1);
        $attrs = $this->reservationRepo->getAllAttributes();

        $this->assertEquals(
            $dateChanged,
            $this->reservationRepo->get('date_end')
        );
        $this->assertEquals(
            'Cancelled',
            $this->reservationRepo->get('status')->getName()
        );
    }

    /**
     * Should remove a reservation
     *
     * @return void
     */
    public function testRemoveReservation()
    {
        $this->reservationRepo->find(1);
        $attrs = $this->reservationRepo->getAllAttributes();

        $this->assertCount(1, $this->reservationRepo->list());

        $this->reservationRepo->delete($attrs['id']);

        $this->assertCount(0, $this->reservationRepo->list());
    }
}
