<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use LaravelDoctrine\ORM\Facades\Doctrine;
use Doctrine\ORM\Configuration;
use Doctrine\DBAL\Connection;
use Doctrine\Common\EventManager;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        Doctrine::extend('default', function(Configuration $configuration, Connection $connection, EventManager $eventManager) {
            //
        });
    }
}
