<?php

namespace App\Entities;

use Doctrine\ORM\Mapping AS ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * @ORM\Entity
 * @ORM\Table(name="brands")
 */
class Brand
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    protected $id = null;

    /**
     * @ORM\Column(type="string", unique=true, nullable=false)
     */
    protected $name = null;

    /**
     * @ORM\OneToMany(targetEntity="Vehicle", mappedBy="brand")
     */
    protected $vehicles;

    public function __construct($name = null)
    {
        if (!empty($name)) $this->name = $name;
        $this->vehicles = new ArrayCollection;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName(String $name)
    {
        $this->name = $name;
    }
}
