<?php

namespace App\Entities;

use Doctrine\ORM\Mapping AS ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="reservations")
 */
class Reservations
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    protected $id = null;

    /**
     * @ORM\Column(type="date")
     */
    protected $date_start = null;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    protected $date_end = null;

    /**
    * @ORM\OneToOne(targetEntity="Vehicle")
    * @ORM\JoinColumn(name="vehicle_id", referencedColumnName="id")
    */
    protected $vehicle = null;

    /**
    * @ORM\OneToOne(targetEntity="Client")
    * @ORM\JoinColumn(name="client_id", referencedColumnName="id")
    */
    protected $client = null;

    /**
    * @ORM\OneToOne(targetEntity="ReservationStatus")
    * @ORM\JoinColumn(name="status_id", referencedColumnName="id")
    */
    protected $status;

    public function getId()
    {
        return $this->id;
    }

    public function setDate_start($date_start)
    {
        $this->date_start = $date_start;
    }

    public function getDate_start()
    {
        return $this->date_start;
    }

    public function setDate_end($date_end)
    {
        $this->date_end = $date_end;
    }

    public function getDate_end()
    {
        return $this->date_end;
    }

    public function setStatus($status)
    {
        $this->status = $status;
    }

    public function getStatus()
    {
        return $this->status;
    }

    public function getVehicle()
    {
        return $this->vehicle;
    }

    public function setVehicle(\App\Entities\Vehicle $vehicle)
    {
        return $this->vehicle = $vehicle;
    }

    public function getClient()
    {
        return $this->client;
    }

    public function setClient(\App\Entities\client $client)
    {
        return $this->client = $client;
    }
}
