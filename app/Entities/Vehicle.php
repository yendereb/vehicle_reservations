<?php

namespace App\Entities;

use Doctrine\ORM\Mapping AS ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="vehicles")
 */
class Vehicle
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    protected $id = null;

    /**
     * @ORM\Column(type="string", unique=true, nullable=false)
     */
    protected $registration = null;

    /**
     * @ORM\Column(type="integer", unique=false, nullable=false)
     */
    protected $year = null;

    /**
     * @ORM\Column(type="string", unique=false, nullable=false)
     */
    protected $model = null;

    /**
     * @ORM\ManyToOne(targetEntity="Brand", inversedBy="vehicles")
     * @ORM\JoinColumn(name="brand_id", referencedColumnName="id")
     */
    protected $brand = null;

    /**
     * @ORM\Column(type="integer", unique=false, nullable=false)
     */
    protected $seats = null;

    /**
     * @ORM\Column(type="string", unique=false, nullable=true)
     */
    protected $conditions = null;

    /**
     * @ORM\Column(type="string", unique=true, nullable=false)
     */
    protected $status = 'available';

    public function getId()
    {
        return $this->id;
    }

    public function setRegistration($registration)
    {
        $this->registration = $registration;
    }

    public function setYear($year)
    {
        $this->year = $year;
    }

    public function setModel($model)
    {
        $this->model = $model;
    }

    public function setSeats($seats)
    {
        $this->seats = $seats;
    }

    public function setConditions($conditions)
    {
        $this->conditions = $conditions;
    }

    public function setStatus($status)
    {
        $this->status = $status;
    }

    public function getRegistration()
    {
        return $this->registration;
    }

    public function getYear()
    {
        return $this->year;
    }

    public function getModel()
    {
        return $this->model;
    }

    public function getSeats()
    {
        return $this->seats;
    }

    public function getConditions()
    {
        return $this->conditions;
    }

    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return Brand|null $brand
     */
    public function getBrand()
    {
        return $this->brand;
    }

    /**
     * @param Brand|null $brand
     */
    public function setBrand(\App\Entities\Brand $brand)
    {
        return $this->brand = $brand;
    }
}
