<?php

namespace App\Entities;

use Doctrine\ORM\Mapping AS ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="clients")
 */
class Client
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    protected $id = null;

    /**
     * @ORM\Column(type="string", unique=false, nullable=false)
     */
    protected $first_name = null;

    /**
     * @ORM\Column(type="string", unique=false, nullable=false)
     */
    protected $last_name = null;

    /**
     * @ORM\Column(type="string", unique=false, nullable=false)
     */
    protected $phone = null;

    /**
     * @ORM\Column(type="text", unique=false, nullable=true)
     */
    protected $address = null;

    /**
     * @ORM\Column(type="string", unique=true, nullable=true)
     */
    protected $code;

    /**
     * @ORM\OneToOne(targetEntity="User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;

    public function getId()
    {
        return $this->id;
    }

    public function setFirst_name($first_name)
    {
        $this->first_name = $first_name;
    }
    public function getFirst_name()
    {
        return $this->first_name;
    }

    public function setLast_name($last_name)
    {
        $this->last_name = $last_name;
    }
    public function getLast_name()
    {
        return $this->last_name;
    }

    public function setPhone($phone)
    {
        $this->phone = $phone;
    }
    public function getPhone()
    {
        return $this->phone;
    }

    public function setAddress($address)
    {
        $this->address = $address;
    }
    public function getAddress()
    {
        return $this->address;
    }

    public function setCode($code)
    {
        $this->code = $code;
    }
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param user|null $user
     */
    public function setUser(\App\Entities\User $user)
    {
        return $this->user = $user;
    }

    /**
     * @return User|null
     */
    public function getUser()
    {
        return $this->user;
    }
}
