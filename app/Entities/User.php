<?php

namespace App\Entities;

use Doctrine\ORM\Mapping AS ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="users")
 */
class User
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    protected $id = null;

    /**
     * @ORM\Column(type="string", unique=true, nullable=false)
     */
    protected $email = null;

    /**
     * @ORM\Column(type="string")
     */
    protected $password = null;

    /**
     * @ORM\Column(type="datetime", unique=false, nullable=true)
     */
    protected $email_verified_at = null;

    /**
     * @ORM\Column(type="string", unique=true, nullable=false)
     */
    protected $api_token = null;

    /**
     * @ORM\Column(type="string", unique=false, nullable=true)
     */
    protected $remember_token = null;

    public function getId()
    {
        return $this->id;
    }

    public function setEmail($email)
    {
        $this->email = $email;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function setPassword($password)
    {
        $this->password = $password;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setEmail_verified_at($email_verified_at)
    {
        $this->email_verified_at = $email_verified_at;
    }

    public function getEmail_verified_at()
    {
        return $this->email_verified_at;
    }

    public function setApi_token($api_token)
    {
        $this->api_token = $api_token;
    }

    public function getApi_token()
    {
        return $this->api_token;
    }

    public function setRemember_token($remember_token)
    {
        $this->remember_token = $remember_token;
    }

    public function getRemember_token()
    {
        return $this->remember_token;
    }
}
