<?php

namespace App\Repositories;

interface EntityRepository
{
    public function getAllAttributes() :array;

    public function create(array  $data);

    public function update(array $data, int $id) :bool;

    public function delete(int $id) :bool;

    public function find(int $id) :object;
}