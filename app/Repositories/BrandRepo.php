<?php
namespace App\Repositories;

class BrandRepo extends BaseRepo
{

    /**
     * @var string
     */
    protected $class = 'App\Entities\Brand';
}