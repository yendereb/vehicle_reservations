<?php
namespace App\Repositories;

class ReservationStatusRepo extends BaseRepo
{

    /**
     * @var string
     */
    protected $class = 'App\Entities\ReservationStatus';
}