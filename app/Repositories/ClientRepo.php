<?php
namespace App\Repositories;

class ClientRepo extends BaseRepo
{

    /**
     * @var string
     */
    protected $class = 'App\Entities\Client';
}