<?php
namespace App\Repositories;

class VehicleRepo extends BaseRepo
{

    /**
     * @var string
     */
    protected $class = 'App\Entities\Vehicle';

    /**
     * Return all availales vehicles
     * 
     * @return array
     */
    public function getAvailables() :array
    {
        return $this->query = \EntityManager::getRepository($this->class)
                ->createQueryBuilder('v')
                ->leftJoin('v.brand', 'b')
                ->select(['v', 'b'])
                ->where('v.status = ?1')
                ->setParameter(1, 'available')
                ->getQuery()
                ->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);
    }
}