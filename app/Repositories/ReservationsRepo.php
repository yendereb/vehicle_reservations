<?php
namespace App\Repositories;

class ReservationsRepo extends BaseRepo
{

    /**
     * @var string
     */
    protected $class = 'App\Entities\Reservations';
}