<?php
namespace App\Repositories;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Persistence\ManagerRegistry;

class BaseRepo implements EntityRepository
{

    /**
     * @var string
     */
    protected $class = '';

    /**
     * @var Entity
     */
    protected $entity = null;

    /**
     * @var QueryBuilder
     */
    protected $query = null;

    /**
     * @var QueryBuilder select
     */
    protected $select = ['r'];

    /**
     * The entity manager
     * 
     * @var EntityManager
     */
    protected $em;


    public function __construct(string $connection = 'default')
    {
        $this->entity = new $this->class;

        $em = \App::make('Doctrine\Common\Persistence\ManagerRegistry');
        $this->em = $em->getManager($connection);
    }

    /**
     * Set the attributes from array
     * 
     * @param  array  $attr
     */
    protected function mapAttributesFromArray(array $attr)
    {
        foreach ($attr as $prop => $value) {
            if (property_exists($this->entity, $prop)) {
                $this->set($prop, $value);
            }
        }
    }

    /**
     * Create a new row of entity
     * 
     * @param  array  $data
     * @return boolean
     */
    public function create(array $data)
    {
        $this->entity = new $this->class();
        if (!isset($data['created_at'])) {
            $data['created_at'] = \Carbon\Carbon::now();
        }

        try {
            $this->mapAttributesFromArray($data);

            $this->save();
        } catch(\Doctrine\DBAL\DBALException $e) {
            \Log::error($e->getMessage());
            return null;
        }

        return $this;
    }

    /**
     * Update a entoty object
     * 
     * @param  array  $data
     * @param  int    $id
     * @return boolean
     */
    public function update(array $data, int $id = null) :bool
    {
        if (!isset($data['updayed_at'])) {
            $data['updayed_at'] = \Carbon\Carbon::now();
        }

        if ($id === null) $id = $this->get('id');

        try {
            $this->find($id);

            if (empty($this->entity)) return false;

            $this->mapAttributesFromArray($data);

            $this->save();
        } catch(\Doctrine\DBAL\DBALException $e) {
            \Log::error($e->getMessage());
            return false;
        }

        return true;
    }

    /**
     * Find a object by id
     * 
     * @param  integer $id
     * @return object
     */
    public function find(int $id) :object
    {
        $this->entity = $this->em->find($this->class, $id);

        return $this;
    }

    /**
     * Find a object by criteria
     * 
     * @param  array $array
     * @return object
     */
    public function findOneBy(array $data) :object
    {
        $this->entity = $this->em->getRepository($this->class)->findOneBy($data);

        return $this;
    }

    /**
     * Return all properties
     * 
     * @return array
     */
    public function getAllAttributes() :array
    {
        $attributes = [];
        if (empty($this->entity)) return $attributes;

        $columns = $this->em->getClassMetadata($this->class)->getColumnNames();
        foreach ($columns as $attribute) {
            $attributes[$attribute] = $this->entity->{'get'.ucfirst($attribute)}();
        }

        return $attributes;
    }

    /**
     * Return all records
     * 
     * @return array
     */
    public function list() :array
    {
        $q = $this->getThisQuery()->select($this->select)->getQuery();
        $result = $q->getResult(\Doctrine\ORM\Query::HYDRATE_ARRAY);

        if (!$result) return [];

        return $result;
    }

    /**
     * Delete a entity object
     * 
     * @param  int    $id
     * @return boolean
     */
    public function delete(int $id) :bool
    {
        $this->find($id);

        if (empty($this->entity)) return false;

        $this->em->remove($this->entity);
        $this->em->flush();

        return true;
    }

    /**
     * Set a single attibute
     * 
     * @param  string $prop
     * @param  mixed  $value
     * @return mixed
     */
    public function set(string $prop, $value)
    {
        if (!property_exists($this->entity, $prop)) {
            return null;
        }

        if(is_object($value) && strpos(get_class($value), 'Repositories')) {
            $this->entity->{'set'.ucfirst($prop)}($value->getEntity());
            return $this;
        }

        $this->entity->{'set'.ucfirst($prop)}($value);

        return $this;
    }

    /**
     * Return a single attibute
     * 
     * @param  string $prop
     * @return mixed
     */
    public function get(string $prop)
    {
        if (!property_exists($this->entity, $prop)) {
            false;
        }
        
        return $this->entity->{'get'.ucfirst($prop)}();
    }

    /**
     * Set the entity object
     * 
     * @return object
     */
    public function setEntity($entity) :object
    {
        $this->entity = $entity;
        return $this;
    }

    /**
     * Save the current entity object
     * 
     * @return object
     */
    public function save() :bool
    {
        if (empty($this->entity)) return false;
        $this->em->persist($this->entity);
        $this->em->flush();
        return true;
    }

    /**
     * Return the entity object
     * 
     * @return object
     */
    public function getEntity()
    {
        return $this->entity;
    }

    /**
     * Add a relation to a queryBuilder object
     * 
     * @param  string $repo
     * @return object
     */
    public function with(string $repo) :object
    {
        $q = $this->getThisQuery();
        $q->leftJoin('r.' . $repo, $repo);
        $this->select[] = $repo;

        return $this;
    }

    /**
     * Return o rgenerate the queryBuilde
     * 
     * @return object
     */
    protected function getThisQuery()
    {
        if (empty($this->query)) {
            $this->query = $this->em->getRepository($this->class)
                ->createQueryBuilder('r');
        }

        return $this->query;
    }
}