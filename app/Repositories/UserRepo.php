<?php
namespace App\Repositories;

class UserRepo extends BaseRepo
{

    /**
     * @var string
     */
    protected $class = 'App\Entities\User';
}