<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\BrandRequest;
use App\Repositories\BrandRepo;

/**
* @group Brand
*
* APIs for managing Brands
*/
class BrandController extends Controller
{
    private $brands;

    public function __construct(BrandRepo $brand)
    {
        $this->brands = $brand;
    }

    /**
     * Display a listing of all brands
     *
     * @response {
     *     "success": true,
     *     "brands": [
     *         "id": 1,
     *         "name": "Audi"
     *     ]
     * }
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $brands = $this->brands->list();

        return response()->json(['success' => true, 'brands' => $brands]);
    }

    /**
     * Store a newly created brand
     *
     * @bodyParam name string required The name of the new Brand
     * @response {
     *     "success": true,
     *     "brand": [...]
     * }
     * @response 404 {
     *      'success' => false,
            'brand'   => []
     * }
     *
     * @param  \Illuminate\Http\Requests\BrandRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(BrandRequest $request)
    {
        $saved = $this->brands->create($request->all());

        return response()->json([
            'success' => empty($saved) ? false : true,
            'brand'   => $this->brands->getAllAttributes()
        ]);
    }

    /**
     * Display the specified brand.
     *
     * @queryParam id required The ID of the brand
     * @response {
     *     "success": true,
     *     "brand": [...]
     * }
     * @response 404 {
     *      'success' => false,
            'error'   => 'Not Found'
     * }
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $brand = $this->brands->find($id)->getAllAttributes();

        if (empty($brand)) {
            return response()->json(['success' => false, 'error' => 'Not found'],404);
        }

        return response()->json(['success' => true, 'brand' =>$brand]);
    }

    /**
     * Update the specified brand
     *
     * @queryParam id required The ID of the brand
     * @bodyParam name string required The name of the new Brand
     * @response {
     *     "success": true
     * }
     * 
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(BrandRequest $request, $id)
    {
        $saved = $this->brands->update($request->all(), $id);

        return response()->json(['success' => $saved]);
    }

    /**
     * Remove the specified brand
     *
     * @queryParam id required The ID of the brand
     * @response {
     *     "success": true
     * }
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(int $id)
    {
        return response()->json([
            'success' => $this->brands->delete($id)]
        );
    }
}
