<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\ReservationRequest;
use App\Http\Requests\ReservationUpdateRequest;
use App\Http\Requests\ReservationStatusRequest;
use App\Repositories\ClientRepo;
use App\Repositories\VehicleRepo;
use App\Repositories\ReservationsRepo;
use App\Repositories\ReservationStatusRepo;
use \Carbon\Carbon;

/**
* @group Reservations
*
* APIs for managing reservations
*/
class ReservationsController extends Controller
{
    /**
     * Client Repo
     * @var Object
     */
    private $client;

    /**
     * Vehicle Repo
     * @var Object
     */
    private $vehicle;

    /**
     * Reservation Repo
     * @var Object
     */
    private $reservation;

    /**
     * ReservationSsatus Repo
     * @var Object
     */
    private $reservationStatus;

    public function __construct()
    {
        $this->client = new ClientRepo();
        $this->vehicle = new VehicleRepo();
        $this->reservation = new ReservationsRepo();
        $this->reservationStatus = new ReservationStatusRepo();
    }

    /**
     * Return a listing of the all reservations
     *
     * Used by show reports of reservations
     *
     * @response {
     *     "success": true,
     *     "reservations": [
     *         "id": 1,
     *         "date_start": "",
     *         "date_end": "",
     *         ...
     *     ]
     * }
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()  :\Illuminate\Http\JsonResponse
    {
        $reservations = $this->reservation
            ->with('vehicle')
            ->with('client')
            ->with('status')
            ->list();

        return response()->json([
            'success' => true,
            'reservations' => $reservations
        ]);
    }

    /**
     * Store a newly created reservation.
     *
     * @bodyParam date_start date required The date of the reservation
     * @bodyParam client_id integer required The ID of the client
     * @bodyParam vehicle_id integer required The ID of the vehicle
     * @bodyParam status_id integer required The ID of the status of reservation
     * @response {
     *     "success": true,
     *     "reservation": [...]
     * }
     * @response 404 {
     *      'success' => false,
            'error'   => 'Vehicle Not Found'
     * }
     * @response 500 {
     *      'success' => false
     * }
     *
     * @param  App\Http\Requests\ReservationRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(ReservationRequest $request)  :\Illuminate\Http\JsonResponse
    {
        $client  = $this->client->find($request->get('client_id'));
        $status  = $this->reservationStatus->find($request->get('status_id') ?? 1);
        $vehicle = $this->vehicle->findOneBy([
            'id'     => $request->get('vehicle_id'),
            'status' => 'available'
        ]);

        // Vehicle not available
        if (!$vehicle->getAllAttributes()) {
            return response()->json(
                [
                    'success' => false,
                    'error' => 'Vehicle Not Found'
                ], 
                404)
            ;
        }

        $reservationData = $request->all();
        $reservationData['date_start'] = Carbon::parse($reservationData['date_start']);
        $reservationData['vehicle'] = $vehicle;
        $reservationData['client']  = $client;
        $reservationData['status']  = $status;

        $saved = $this->reservation->create($reservationData);

        if (!$saved) {
            return response()->json(['success' => false], 500);
        }

        $vehicle->update(['status' => 'reserved']);

        return response()->json([
            'success' => true, 
            'reservation' => $this->reservation->getAllAttributes()
        ]);
    }

    /**
     * Display a specified reservation.
     * 
     * @queryParam id required The ID of the reservation
     * @response {
     *     "success": true,
     *     "reservation": [...]
     * }
     * @response 404 {
     *      'success' => false,
            'error'   => 'Not Found'
     * }
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)  :\Illuminate\Http\JsonResponse
    {
        $reservation = $this->reservation->find($id);
        $attributes  = $reservation->getAllAttributes();

        if (empty($attributes)) {
            return response()->json(
                [
                    'success' => false,
                    'error' => 'Not found'
                ],
                404
            );
        }

        // Vehicle, Client and Status data
        $attributes['vehicle'] = $this->vehicle
            ->find($reservation->get('vehicle')->getId())
            ->getAllAttributes();
        $attributes['client'] = $this->client
            ->find($reservation->get('client')->getId())
            ->getAllAttributes();
        $attributes['status'] = $this->reservationStatus
            ->find($reservation->get('status')->getId())
            ->getAllAttributes();

        return response()->json([
            'success' => true,
            'reservation' =>$attributes
        ]);
    }

    /**
     * Update the specified reservation
     *
     * @queryParam id required The ID of the reservation
     * @bodyParam date_end date required The date when reservation finalize
     * @bodyParam status_id integer required The ID of the status of reservation
     * @response {
     *     "success": true
     * }
     *
     * @param  App\Http\Requests\ReservationUpdateRequest $request
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(ReservationUpdateRequest $request, $id)  :\Illuminate\Http\JsonResponse
    {
        $data= [
            'date_end' => Carbon::parse($request->get('date_end')),
            'status'   => $this->reservationStatus->find($request->get('status_id'))
        ];

        $saved = $this->reservation->update($data, $id);

        return response()->json(['success' => $saved]);
    }

    /**
     * Remove the specified reservation
     *
     * @queryParam iid required The ID of the reservation
     * @response {
     *     "success": true
     * }
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)  :\Illuminate\Http\JsonResponse
    {
        return response()->json([
            'success' => $this->reservation->delete($id)]
        );
    }

    /**
     * Return all available vehicles to reservations
     *
     * @response {
     *     "success": true
     *     "vehicles": []
     * }
     * 
     * @return \Illuminate\Http\JsonResponse
     */
    public function availables()  :\Illuminate\Http\JsonResponse
    {
        $vehicles = $this->vehicle->getAvailables();

        return response()->json(['success' => true, 'vehicles' => $vehicles]);
    }

    /**
     * Change the status of the reservation with given in request
     *
     * Used by aprove, decline, cancel or end a reservation
     *
     * @bodyParam id integer required The reservation id
     * @bodyParam status string required The new status name Example: Approved
     * @response {
     *     "success": true
     * }
     * @response 500 {
     *     "success": false
     * }
     * 
     * @param  ReservationStatusRequest $request [description]
     * @return \Illuminate\Http\JsonResponse
     */
    public function changeStatus(ReservationStatusRequest $request) :\Illuminate\Http\JsonResponse
    {
        $reservationId = $request->get('id');
        $newStatus     = $request->get('status');
        $request = new ReservationUpdateRequest();

        $request->merge([
            'status_id' => $this->reservationStatus
                ->findOneBy(['name' => $newStatus])->get('id')
        ]);

        return $this->update($request, $reservationId);
    }
}
