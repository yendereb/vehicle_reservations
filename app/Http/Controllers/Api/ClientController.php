<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\ClientRequest;
use App\Repositories\ClientRepo;
use App\Repositories\UserRepo;

/**
* @group Client
*
* APIs for managing Clients
*/
class ClientController extends Controller
{
    /**
     * User Repo
     * @var Object
     */
    private $user;

    /**
     * Client Repo
     * @var Object
     */
    private $client;

    public function __construct()
    {
        $this->client = new ClientRepo();
        $this->user = new UserRepo();
    }

    /**
     * Display a listing of the clients
     *
     * @response {
     *     "success": true,
     *     "clients": [
     *         {
                    "id": 1,
                    "first_name": "Rickie",
                    "last_name": "Romaguera",
                    "phone": "+1 (603) 995-2922",
                    "address": "16952 Pedro Highway\nHermistonville, AZ 78630",
                    "code": "S3XfE5sG"
                },
                {...}
     *     ]
     * }
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $clients = $this->client->list();

        return response()->json(['success' => true, 'clients' => $clients]);
    }

    /**
     * Store a newly created client
     *
     * @bodyParam first_name string required The name of the client
     * @bodyParam last_name string required The last name of the client
     * @bodyParam phone string optional The phone of the client
     * @bodyParam address string optional The address of the vehicle
     * @bodyParam user_id integer required The ID of the user linked to the client
     * @response {
     *     "success": true,
            "client": {
                "id": 1,
                "first_name": "Rickie",
                "last_name": "Romaguera",
                "phone": "+1 (603) 995-2922",
                "address": "16952 Pedro Highway\nHermistonville, AZ 78630",
                "code": "S3XfE5sG"
            }
     * }
     * @response 404 {
     *      'success' => false,
            'client'  => []
     * }
     *
     * @param  \App\Http\Requests\ClientRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(ClientRequest $request)
    {
        $user = $this->user->find($request->get('user_id'));
        $clientData = $request->all();
        $clientData['user'] = $this->user;
        $saved = $this->client->create($clientData);

        return response()->json([
            'success' => empty($saved) ? false : true,
            'client'  => $this->client->getAllAttributes()
        ]);
    }

    /**
     * Display the specified client
     *
     * @queryParam id required The ID of the client
     * @response {
     *     "success": true,
            "client": {
                "id": 1,
                "first_name": "Rickie",
                "last_name": "Romaguera",
                "phone": "+1 (603) 995-2922",
                "address": "16952 Pedro Highway\nHermistonville, AZ 78630",
                "code": "S3XfE5sG"
            }
     * }
     * @response 404 {
     *      'success' => false,
            'error'   => 'Not Found'
     * }
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $client = $this->client->find($id)->getAllAttributes();

        if (empty($client)) {
            return response()->json(['success' => false, 'error' => 'Not found'],404);
        }

        return response()->json(['success' => true, 'client' =>$client]);
    }

    /**
     * Update the specified client in storage
     *
     * @queryParam id required The ID of the client
     * @bodyParam first_name string required The name of the client
     * @bodyParam last_name string required The last name of the client
     * @bodyParam phone string optional The phone of the client
     * @bodyParam address string optional The address of the vehicle
     * @bodyParam user_id integer required The ID of the user linked to the client
     * @response {
     *     "success": true
     * }
     * @response 400{
     *     "success": false
     * }
     *
     * @param  \App\Http\Requests\ClientRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(ClientRequest $request, $id)
    {
        $user = $this->user->find($request->get('user_id'));
        $clientData = $request->all();
        $clientData['user'] = $this->user;

        $saved = $this->client->update($clientData, $id);

        return response()->json(['success' => $saved]);
    }

    /**
     * Remove the specified client
     *
     * @queryParam id required The ID of the client
     * @response {
     *     "success": true
     * }
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        return response()->json([
            'success' => $this->client->delete($id)]
        );
    }
}
