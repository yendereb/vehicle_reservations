<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\VehicleRequest;
use App\Repositories\BrandRepo;
use App\Repositories\VehicleRepo;

/**
* @group Vehicle
*
* APIs for managing Vehicles
*/
class VehicleController extends Controller
{
    /**
     * Brand Repo
     * @var Object
     */
    private $brand;

    /**
     * Vehicle Repo
     * @var Object
     */
    private $vehicle;

    public function __construct()
    {
        $this->brand = new BrandRepo();
        $this->vehicle = new VehicleRepo();
    }

    /**
     * Display a listing of the vehicles
     *
     * @response {
         * "success": true,
            "vehicles": [
                {
                    "id": 1,
                    "registration": "UtV3zw",
                    "year": 1984,
                    "model": "sedan",
                    "seats": 5,
                    "conditions": null,
                    "status": "available",
                    "brand": {
                        "id": 1,
                        "name": "Audi"
                    }
                },
                {...}
     * }
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        $vehicles = $this->vehicle->with('brand')->list();

        return response()->json(['success' => true, 'vehicles' => $vehicles]);
    }

    /**
     * Store a newly created vehicle
     *
     * @bodyParam registration string required The registration of the vehicle
     * @bodyParam year integer required The year of the vehicle
     * @bodyParam model string optional The model of the vehicle
     * @bodyParam seats integer optional The seats number of the vehicle
     * @bodyParam brand_id integer required The ID of the brand of the vehicle
     * @bodyParam status string required The status of the vehicle (available,reserved or discontinued)
     * @response {
     *     "success": true,
           "vehicle": {
                "id": 1,
                "registration": "UtV3zw",
                "year": 1984,
                "model": "sedan",
                "seats": 5,
                "conditions": null,
                "status": "available",
                "brand": {
                    "id": 1,
                    "name": "Audi"
                }
            }
     * }
     * @response 404 {
     *      'success' => false,
            'vehicle'  => []
     * }
     *
     * @param  \Illuminate\Http\VehicleRequest  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(VehicleRequest $request)
    {
        $brand = $this->brand->find($request->get('brand_id'));

        $vehicleData = $request->all();
        $vehicleData['brand'] = $this->brand;

        $saved = $this->vehicle->create($vehicleData);

        return response()->json([
            'success' => empty($saved) ? false : true,
            'vehicle' => $this->vehicle->getAllAttributes()
        ]);
    }

    /**
     * Display the specified vehicle
     *
     * @queryParam id required The ID of the vehicle
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show($id)
    {
        $vehicle = $this->vehicle->find($id);
        $vechicleData = $vehicle->getAllAttributes();

        if (empty($vechicleData)) {
            return response()->json(['success' => false, 'error' => 'Not found'],404);
        }

        // Add brand Data
        $vechicleData['brand'] = $this->brand
            ->find($vehicle->get('brand')->getId())
            ->getAllAttributes();

        return response()->json(['success' => true, 'vehicle' =>$vechicleData]);
    }

    /**
     * Update the specified vehicle
     *
     * @queryParam id required The ID of the vehicle
     * @bodyParam registration string required The registration of the vehicle
     * @bodyParam year integer required The year of the vehicle
     * @bodyParam model string optional The model of the vehicle
     * @bodyParam seats integer optional The seats number of the vehicle
     * @bodyParam brand_id integer required The ID of the brand of the vehicle
     * @bodyParam status string required The status of the vehicle (available,reserved or discontinued)
     * @response {
     *     "success": true
     * }
     * @response 400{
     *     "success": false
     * }
     *
     * @param  \Illuminate\Http\VehicleRequest  $request
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(VehicleRequest $request, $id)
    {
        $brand = $this->brand->find($request->get('brand_id'));
        $saved = $this->vehicle->update($request->all(), $id);
        $this->vehicle->set('brand', $brand);

        return response()->json(['success' => $saved]);
    }

    /**
     * Remove the specified vehicle
     *
     * @queryParam id required The ID of the vehicle
     * @response {
     *     "success": true
     * }
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        return response()->json([
            'success' => $this->vehicle->delete($id)]
        );
    }
}
