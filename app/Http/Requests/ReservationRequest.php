<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ReservationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'date_start' => 'required|date_format:Y-m-d',
            'date_end'   => 'date_format:Y-m-d',
            'status_id'  => 'exists:App\Entities\ReservationStatus,id',
            'client_id'  => 'required|integer|exists:App\Entities\Client,id',
            'vehicle_id' => 'required|integer|exists:App\Entities\Vehicle,id'
        ];
    }
}
