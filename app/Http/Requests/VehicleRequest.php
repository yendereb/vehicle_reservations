<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class VehicleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $thisID = \Request::segments()[2] ?? null;
        return [
            'registration' => 'required|string|unique:App\Entities\Vehicle,registration,' . $thisID,
            'year'         => 'required|integer',
            'model'        => 'required|string',
            'seats'        => 'required|integer',
            'status'       => 'required|in:available,reserved,discontinued',
            'brand_id'     => 'required|integer|exists:App\Entities\Brand,id'
        ];
    }
}
