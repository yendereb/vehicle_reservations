<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ClientRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    protected function prepareForValidation()
    {
        if (!$this->has('code')) {
            $this->merge(['code' => uniqid()]);
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $thisID = \Request::segments()[2] ?? null;
        return [
            'first_name' => 'required|string',
            'last_name'  => 'required|string',
            'phone'      => 'required|integer',
            'address'    => 'string',
            'code'       => 'required|string|unique:App\Entities\Client,code,' . $thisID,
            'user_id'    => 'required|integer'
        ];
    }
}
