<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Entities\Client;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(Client::class, function (Faker $faker) {
    return [
        'first_name' => $faker->firstName,
        'last_name'  => $faker->lastName,
        'phone'      => $faker->phoneNumber,
        'address'    => $faker->address,
        'code'       => Str::random(8)
    ];
});
