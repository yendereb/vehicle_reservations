<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Entities\Vehicle;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(Vehicle::class, function (Faker $faker) {
    return [
        'registration' => Str::random(6),
        'year'         => $faker->numberBetween($min = 1980, $max = (int)date('Y')),
        'model'        => $faker->randomElement(['sedan', 'truck', 'family']),
        'seats'        => $faker->numberBetween($min = 5, $max = 8),
        'status'       => 'available'
    ];
});
