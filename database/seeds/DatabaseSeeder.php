<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            BrandsSeeder::class,
            VehiclesSeeder::class,
            ClientSeeder::class,
            ReservationStatusSeeder::class
        ]);
    }
}
