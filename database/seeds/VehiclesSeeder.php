<?php

use Illuminate\Database\Seeder;
use App\Repositories\BrandRepo;
use App\Repositories\VehicleRepo;

class VehiclesSeeder extends Seeder
{
    protected $brandRepo;
    protected $vehicleRepo;

    function __construct()
    {
        $dbConnection = 'default';

        if(\App::runningInConsole() && !empty($_SERVER['argv'])) {
            foreach($_SERVER['argv'] as $arg) {
                if (
                    strpos($arg, '--database') >= 0 && 
                    substr($arg, 11)=='sqlite'
                ) {
                    $dbConnection = 'sqlite';
                }
            }
        }

        $this->brandRepo = new BrandRepo($dbConnection);
        $this->vehicleRepo = new VehicleRepo($dbConnection);
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Brand 1
        entity(App\Entities\Vehicle::class, 3)->make([
            'brand' => $this->brandRepo->find(1)->getEntity()
        ])->each(function ($v) {
            $this->vehicleRepo->setEntity($v)->save();
        });

        // Brand 2
        entity(App\Entities\Vehicle::class, 2)->make([
            'brand' => $this->brandRepo->find(2)->getEntity()
        ])->each(function ($v) {
            $this->vehicleRepo->setEntity($v)->save();
        });

        // Brand 3
        entity(App\Entities\Vehicle::class, 3)->make([
            'brand' => $this->brandRepo->find(3)->getEntity()
        ])->each(function ($v) {
            $this->vehicleRepo->setEntity($v)->save();
        });

        // Brand 4
        entity(App\Entities\Vehicle::class, 2)->make([
            'brand' => $this->brandRepo->find(4)->getEntity()
        ])->each(function ($v) {
            $this->vehicleRepo->setEntity($v)->save();
        });
    }
}
