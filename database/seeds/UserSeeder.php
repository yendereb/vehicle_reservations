<?php

use Illuminate\Database\Seeder;
use App\Repositories\UserRepo;

class UserSeeder extends Seeder
{
    protected $userRepo;

    function __construct()
    {
        $dbConnection = 'default';

        if(\App::runningInConsole() && !empty($_SERVER['argv'])) {
            foreach($_SERVER['argv'] as $arg) {
                if (
                    strpos($arg, '--database') >= 0 && 
                    substr($arg, 11) == 'sqlite'
                ) {
                    $dbConnection = 'sqlite';
                }
            }
        }

        $this->userRepo = new UserRepo($dbConnection);
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        entity(App\Entities\User::class, 3)->create();
    }
}
