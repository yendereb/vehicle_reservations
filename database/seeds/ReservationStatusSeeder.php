<?php

use Illuminate\Database\Seeder;

class ReservationStatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('reservation_statuses')->insert([
            ['name' => 'Requested'],
            ['name' => 'Approved'],
            ['name' => 'Declined'],
            ['name' => 'Cancelled'],
            ['name' => 'Ended']
        ]);
    }
}
