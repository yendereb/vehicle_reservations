<?php

use Illuminate\Database\Seeder;
use App\Repositories\ClientRepo;
use App\Repositories\UserRepo;
use Illuminate\Support\Facades\Hash;
use App\User;

class ClientSeeder extends Seeder
{
    protected $clientRepo;
    protected $userRepo;

    function __construct()
    {
        $dbConnection = 'default';

        if(\App::runningInConsole() && !empty($_SERVER['argv'])) {
            foreach($_SERVER['argv'] as $arg) {
                if (
                    strpos($arg, '--database') >= 0 && 
                    substr($arg, 11) == 'sqlite'
                ) {
                    $dbConnection = 'sqlite';
                }
            }
        }

        $this->clientRepo = new ClientRepo($dbConnection);
        $this->userRepo = new UserRepo($dbConnection);
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $admin = $this->userRepo->create([
            'email' => 'admin@admin.com',
            'email_verified_at' => now(),
            'password' => Hash::make('password'),
            'remember_token' => Str::random(10),
            'api_token' => Str::random(80)
        ]);

        $clientAdmin = entity(App\Entities\Client::class)->make();
        $clientAdmin = $this->clientRepo->setEntity($clientAdmin);
        $clientAdmin->set('user', $admin)->save();

        for ($i = 0; $i < 2; $i++) {
            $this->userRepo->setEntity(
                entity(App\Entities\User::class)->make()
            )->save();

            $client = $this->clientRepo->setEntity(
                entity(App\Entities\Client::class)->make()
            )
            ->set('user', $this->userRepo)
            ->save();
        }
    }
}
