<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::resource('brands', 'Api\BrandController');
Route::resource('vehicles', 'Api\VehicleController');
Route::resource('clients', 'Api\ClientController');

Route::get('reservations/availables', 'Api\ReservationsController@availables');
Route::post('reservations/change', 'Api\ReservationsController@changeStatus');
Route::resource('reservations', 'Api\ReservationsController');